 @extends('layouts.master')
 @section('content')
 
 
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="{{asset('assets/image/slide1.png')}}" alt="First slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="{{asset('assets/image/slide2.jpg')}}" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="{{asset('assets/image/slide3.jpg')}}" alt="Third slide">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <div class="Personal pt-5">
    <div class="container">
      <center><h3>Personal Protective Equipment</h3></center>
      <div class="row">
        <div class="col-md-3 pt-3">
          <div class="card" style="width: 15rem;">
      <img class="card-img-top" width="100" height="200" src="{{asset('assets/image/kids-mask.jpg')}}" alt="Card image cap">
      <div class="card-body">
        <p class="card-text">Kids Hat Mask 1'S</p>
        <p class="price">Rs.500</p>
      </div>
    </div>
        </div>
        <div class="col-md-3 pt-3">
          <div class="card" style="width: 15rem;">
      <img class="card-img-top" width="100" height="200" src="{{asset('assets/image/gloves.jpg')}}" alt="Card image cap">
      <div class="card-body">
        <p class="card-text">Gloves</p>
        <p class="price">Rs.30</p>
      </div>
    </div>
        </div>
        <div class="col-md-3 pt-3">
          <div class="card" style="width: 15rem;">
      <img class="card-img-top" width="100" height="200" src="{{asset('assets/image/head-gear.jpg')}}" alt="Card image cap">
      <div class="card-body">
        <p class="card-text">Head Gear</p>
        <p class="price">Rs.100</p>
      </div>
    </div>
        </div>
        <div class="col-md-3 pt-3">
          <div class="card" style="width: 15rem;">
      <img class="card-img-top" width="100" height="200" src="{{asset('assets/image/kid-face.jpg')}}" alt="Card image cap">
      <div class="card-body">
        <p class="card-text">Kids Face Shield</p>
        <p class="price">Rs.150</p>
      </div>
    </div>
        </div>
      </div>
      <div class="row">
          <div class="Viewbtn pt-3">
            <a href="#" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">View All</a>
          </div>
      </div>
    </div>
  </div>

  <div class="Personal pt-5">
    <div class="container">
      <center><h3>Pain Relief</h3></center>
      <div class="row">
        <div class="col-md-3 pt-3 ">
          <div class="card" style="width: 15rem;">
      <img class="card-img-top" width="100" height="250" src="{{asset('assets/image/balm.jpg')}}" alt="Card image cap">
      <div class="card-body">
        <p class="card-text">Dvago Analgesic Balm 30g</p>
        <p class="price">Rs.180</p>
      </div>
    </div>
        </div>
        <div class="col-md-3 pt-3">
          <div class="card" style="width: 15rem;">
      <img class="card-img-top" width="100" height="250" src="{{asset('assets/image/oxide.jpg')}}" alt="Card image cap">
      <div class="card-body">
        <p class="card-text">Dvago Zinc Oxide Paste 30g</p>
        <p class="price">Rs.180</p>
      </div>
    </div>
        </div>
        <div class="col-md-3 pt-3">
          <div class="card" style="width: 15rem;">
      <img class="card-img-top" width="100" height="250" src="{{asset('assets/image/brufen.jpg')}}" alt="Card image cap">
      <div class="card-body">
        <p class="card-text">Brufen Cream 30gs</p>
        <p class="price">Rs.75</p>
      </div>
    </div>
        </div>
        <div class="col-md-3 pt-3">
          <div class="card" style="width: 15rem;">
      <img class="card-img-top" width="100" height="250" src="{{asset('assets/image/voltral.jpg')}}" alt="Card image cap">
      <div class="card-body">
        <p class="card-text">Voltral Emulgel 2% 40g</p>
        <p class="price">Rs.350</p>
      </div>
    </div>
        </div>
      </div>
      <div class="row">
          <div class="Viewbtn pt-3">
            <a href="#" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">View All</a>
          </div>
      </div>
    </div>
  </div>

  <div class="Personal pt-5">
    <div class="container">
      <center><h3>Devices & Appliances</h3></center>
      <div class="row">
        <div class="col-md-3 pt-3 ">
          <div class="card" style="width: 15rem;">
      <img class="card-img-top" width="100" height="200" src="{{asset('assets/image/device1.jpg')}}" alt="Card image cap">
      <div class="card-body">
        <p class="card-text">Push Pulse Oximeter Ip-22</p>
        <p class="price">Rs.2900</p>
      </div>
    </div>
        </div>
        <div class="col-md-3 pt-3">
          <div class="card" style="width: 15rem;">
      <img class="card-img-top" width="100" height="200" src="{{asset('assets/image/device2.jpg')}}" alt="Card image cap">
      <div class="card-body">
        <p class="card-text">ACCU-CHECK PERFORMA 50 STRIPS</p>
        <p class="price">Rs.1375</p>
      </div>
    </div>
        </div>
        <div class="col-md-3 pt-3">
          <div class="card" style="width: 15rem;">
      <img class="card-img-top" width="100" height="200" src="{{asset('assets/image/device3.jpg')}}" alt="Card image cap">
      <div class="card-body">
        <p class="card-text">Accu-Chek Performa 100 Strips 50x2</p>
        <p class="price">Rs.2500</p>
      </div>
    </div>
        </div>
        <div class="col-md-3 pt-3">
          <div class="card" style="width: 15rem;">
      <img class="card-img-top" width="100" height="200" src="{{asset('assets/image/device4.jpg')}}" alt="Card image cap">
      <div class="card-body">
        <p class="card-text">Accu-Chek Active 50 Strips</p>
        <p class="price">Rs.1350</p>
      </div>
    </div>
        </div>
      </div>
      <div class="row">
          <div class="Viewbtn pt-3">
            <a href="#" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">View All</a>
          </div>
      </div>
    </div>
  </div>

    </div>

    <div class="footer mt-5 pt-3">
      <div class="container">
        <div class="row pt-3">
          <div class="col-md-3">
            <h3 class="foot-heading">DVAGO Pharmacy & Wellness Experts</h3>
            <p class="foot-para">1st Floor, Plot No. 1 Shaheed-e-Millat Road,</p>
            <p class="foot-para">Modern Society MCHS,</p>
            <p class="foot-para">Karachi, Sindh 75100, Pakistan</p>
            <p class="foot-para">phone: (021) 11 11 DVAGO (38246)</p>
            <p class="foot-para">email: cs@dvago.pk</p>
          </div>

          <div class="col-md-3">
            <h3 class="foot-heading">Navigate</h3>
            <a href="#"><p class="foot-para">Home</p></a>
            <a href="#"><p class="foot-para">About</p></a>
            <a href="#"><p class="foot-para">Our Stores</p></a>
            <a href="#"><p class="foot-para">News</p></a>
            <a href="#"><p class="foot-para">Carrer</p></a>
            <a href="#"><p class="foot-para">Blog</p></a>
            <a href="#"><p class="foot-para">Contact Us</p></a>
          </div>
          <div class="col-md-3">
            <h3 class="foot-heading">Categories</h3>
            <a href="#"><p class="foot-para">Medicine</p></a>
            <a href="#"><p class="foot-para">Baby and Mother Care</p></a>
            <a href="#"><p class="foot-para">Personal Care</p></a>
            <a href="#"><p class="foot-para">OTC and Health Needs</p></a>
            <a href="#"><p class="foot-para">Foods and Beverages</p></a>
            <a href="#"><p class="foot-para">Nutrition and Supplements</p></a>
            <a href="#"><p class="foot-para">Devices and Appliances</p></a>
          </div>
          <div class="col-md-3">
            <h3 class="foot-heading">Support</h3>
            <a href="#"><p class="foot-para">FAQs</p></a>
            <a href="#"><p class="foot-para">Card Discounts</p></a>
            <a href="#"><p class="foot-para">Privacy Policy</p></a>
            <a href="#"><p class="foot-para">Terms of Service</p></a>
            <a href="#"><p class="foot-para">Return Policy</p></a>
            <a href="#"><p class="foot-para">Shipping Terms</p></a>
          </div>
        </div>
        <div class="row pt-3">
          <p class="copyright">Copyright © 2021 DVAGO®.</p>
        </div>
      </div>
    </div>
@endsection